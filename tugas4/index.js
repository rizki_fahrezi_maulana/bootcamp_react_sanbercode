// soal 1
console.log("--soal 1 --")
for (let index = 0; index < 10; index++) {
    console.log(index);
}
// soal 2
console.log("--soal 2 --")
for (let index = 0; index < 10; index++) {
    if ((index % 2) != 0) {
        console.log(index);
    }
}
// soal 3
console.log("--soal 3 --")
for (let index = 0; index < 10; index++) {
    if ((index % 2) == 0) {
        console.log(index);
    }
}
// soal 4
console.log("--soal 4--")
let array1 = [1, 2, 3, 4, 5, 6];
console.log(array1[5]);
// soal 5
console.log("--soal 5--");
let array2 = [5, 2, 4, 1, 3, 5];
console.log(array2.sort());
// soal 6
console.log("--soal 6--");
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"];
let string = "";
for (let index = 0; index < array3.length; index++) {
    string += array3[index] + " ";
}
console.log(string);
// soal 7
console.log("--soal 7--");
let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
for (let index = 0; index < array4.length; index++) {
    if ((array4[index] % 2) == 0) {
        console.log(array4[index]);
    }
}
// soal 8
console.log("--soal 8--");
let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];
console.log(kalimat.join(" "));
// soal 9
console.log("--soal 9--");
var sayuran = [];

sayuran.push("Kangkung");
sayuran.push("Bayam");
sayuran.push("Buncis");
sayuran.push("Kubis");
sayuran.push("Timun");
sayuran.push("Seledri");
sayuran.push("Tauge");

console.log(sayuran);
