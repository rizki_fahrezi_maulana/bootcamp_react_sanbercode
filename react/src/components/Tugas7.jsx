import React from 'react'

const Tugas7 = (props) => {
    return (
        <>
            <div className="article">
                <h3>Nama : {props.name}</h3>
                <h4>Batch : {props.batch}</h4>
                <h4>Email : {props.email}</h4>
            </div>
        </>
    )
}
export default Tugas7;