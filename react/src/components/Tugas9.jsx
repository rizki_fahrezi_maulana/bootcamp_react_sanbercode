import React, { useState } from 'react'
import { Table } from 'flowbite-react';

const Tugas9 = () => {
    return (
        <>
            <div className="article">
                <Table>
                    <Table.Head>
                        <Table.HeadCell className="bg-violet-300">
                            No
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Nama
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Mata Kuliah
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Nilai
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Index Nilai
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Action
                        </Table.HeadCell>
                    </Table.Head>
                    <Table.Body className="divide-y">
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                1
                            </Table.Cell>
                            <Table.Cell>
                                Rizki Fahrezi Maulana
                            </Table.Cell>
                            <Table.Cell>
                                Basis Data
                            </Table.Cell>
                            <Table.Cell>
                                80
                            </Table.Cell>
                            <Table.Cell>
                                B
                            </Table.Cell>
                            <Table.Cell>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Edit
                                    </p>
                                </a>
                                <a
                                    className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                    href="/tables"
                                >
                                    <p>
                                        Hapus
                                    </p>
                                </a>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </div>
        </>
    )
}

export default Tugas9