import React, { useState } from 'react'

const Tugas8 = () => {
    const [count, setCount] = useState(0);
    const handleCount = () => {
        if (count > 10) {

        }
        console.log("OK");
        setCount(count + 1);
    }
    return (
        <>
            <div className="article">
                    <h3 className='textcenter'>{count}</h3>
                    {count > 10 &&
                        <h4>Data lebih dari 10</h4>
                    }
                <button onClick={handleCount}>Klik</button>
            </div>
        </>
    )
}

export default Tugas8