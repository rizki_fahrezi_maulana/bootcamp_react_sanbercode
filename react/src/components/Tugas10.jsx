import React from 'react'
import { useState, useEffect } from 'react';
import { Table } from 'flowbite-react';

const Tugas10 = () => {
    const [students, setStudents] = useState([]);
    const fetchSiswa = () => {
        fetch("https://backendexample.sanbercloud.com/api/student-scores")
            .then(response => {
                return response.json()
            })
            .then(data => {
                setStudents(data)
                console.log(students);
            })
    }
    useEffect(() => {
        fetchSiswa();

    })
    return (
        <>
            <div className="article">
                <h1 className='text-center'>REACT CRUD FETCHING DATA</h1>
                <Table>
                    <Table.Head>
                        <Table.HeadCell className="bg-violet-300">
                            No
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Nama
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Mata Kuliah
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Nilai
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Index Nilai
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-violet-300">
                            Action
                        </Table.HeadCell>
                    </Table.Head>
                    <Table.Body className="divide-y">
                        {students.map((student) => {
                            let indexValue;
                            if (student.score >= 80) {
                                indexValue = 'A';
                            } else if (student.score >= 70) {
                                indexValue = 'B';
                            } else if (student.score >= 60) {
                                indexValue = 'C';
                            } else if (student.score >= 50) {
                                indexValue = 'D';
                            } else {
                                indexValue = 'E';
                            }
                            return (
                                <Table.Row key={student.id} className="bg-white dark:border-gray-700 dark:bg-gray-800">
                                    <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                        1
                                    </Table.Cell>
                                    <Table.Cell>
                                        {student.name}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {student.course}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {student.score}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {indexValue}
                                    </Table.Cell>
                                    <Table.Cell>
                                        <a
                                            className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                            href="/tables"
                                        >
                                            <p>
                                                Edit
                                            </p>
                                        </a>
                                        <a
                                            className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                            href="/tables"
                                        >
                                            <p>
                                                Hapus
                                            </p>
                                        </a>
                                    </Table.Cell>
                                </Table.Row>
                            )
                        })}
                    </Table.Body>
                </Table>
            </div>
        </>
    )
}

export default Tugas10