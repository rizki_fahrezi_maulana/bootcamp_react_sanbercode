import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Tugas6, Tugas8, Tugas9 } from './components'
import { Tugas7 } from './components'
import Tugas10 from './components/Tugas10'
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <Tugas10 />
      <Tugas6 />
      <Tugas7 name="Rizki Fahrezi Maulana" batch="50" email="rizkifahrezi990@gmail.com" />
      <Tugas8 />
      <Tugas9 />
    </>
  )
}

export default App
